import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Patient from "../views/Patient.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: Home
  },
  {
    path:'/patients/:id',
    name:'patient',
    component:Patient,
    props:true,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
