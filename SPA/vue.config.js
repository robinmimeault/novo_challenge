module: {
    loaders: [
      {
        test: /\.vue$/,
        loader: 'vue'
      }, 
      {
          test: /\.s[a|c]ss$/,
          loader: 'style!css!sass',
          options: {
            sourceMap: true,
          },
      }
    ]
  }
  vue: {
    loaders: {
      scss: 'style!css!sass'
    }
  }