const express = require('express');
const cors = require('cors');
const app = express();
const routes = require('./routes');

app.use(cors());

// Express Middleware, expect response as JSON on a property called body
app.use(express.json());

app.use('/api',routes);


//Middleware for handling non existing routes
app.use((req,res,next) => {
  const err = new Error('Not found');
  err.status = 404;
  next(err);
});



app.use((err,req,res,next) => {
  res.status(err.status || 500);
  res.json({
    error: err.message
  });
});

app.listen(3000, () => console.log('Quote API listening on port 3000!'));
