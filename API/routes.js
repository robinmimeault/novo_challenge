const express = require('express');
const router = express.Router();
const records = require('./records');


function asyncHandler(cb){
    return async (req,res, next) => {
        try {
            await cb(req, res, next);
        } catch(err) {
            next(err);
        }
    }
}


// Send a GET request to /quotes to READ a list quote
router.get('/patients', async (req, res)=>{
    try {
      const patients = await records.getPatients();
      res.json(patients)
    } catch (err) {
      res.status(500).json({message: err.message});
    }
});

  
// Send a GET request to /patients/:id to READ (view) a patient
router.get('/patients/:id', async (req, res) => {
try {
    const patient = await records.getPatient(req.params.id);
    if(patient){
    res.json(patient);
    }else{
    res.status(404).json({message: "Quote not found"});
    }
} catch (err) {
    res.status(500).json({message: err.message});
} 
});
  
  // Send a POST request to /quotes to CREATE a quote
  // app.post('/quotes', async (req,res) =>{
  //   try{
  //     //throw new Error("Oh oh, something went wrong");
  //     if (req.body.quote && req.body.author) {
  //       const quote = await records.createQuote({
  //         quote: req.body.quote,
  //         author: req.body.author
  //       });
  //       res.status(201).json(quote);
  //     }else{
  //       res.status(400).json({message: "Quote and author required."});
  //     }
  
  //   }catch(err){
  //     res.status(500).json({message: err.message});
  //   }
  // });
  
  
  // Send a POST request to /quotes to CREATE a quote
  router.post('/patients', asyncHandler( async(req,res) => {
    
    // validate if fields aren't empty
    if (req.body.firstname && req.body.lastname) {

      // Generate a random ID for the avatar
      const avatar_ID = Math.floor(Math.random() * 40) + 1;

      const patient = await records.createPatient({
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        age: req.body.age,
        sexe: req.body.sexe,
        email: req.body.email,
        avatar: "https://randomuser.me/api/portraits/" + req.body.sexe +"/" + avatar_ID + ".jpg"
      });
      res.status(201).json(patient);
    }else{
      res.status(400).json({message: "Firstname and lastname required."});
    }
  }));
  
  // Send a PUT request to /quotes/:id to UPDATE (edit) a quote
  router.put('/patients/:id', asyncHandler( async(req,res) => {
    const patient = await records.getPatient(req.params.id);
    if(patient){
      patient.lastname = req.body.lastname;
      patient.firstname = req.body.firstname;
      patient.age = req.body.age;
      patient.sexe = req.body.sexe;
      patient.email = req.body.email;
  
        await records.updatePatient(patient);
        res.status(204).end();
    } else {
        res.status(404).json({message: "Patient Not Found"});
    }
  }));
  
  // Send a DELETE request to /quotes/:id to DELETE a quote
  router.delete('/patients/:id', async(req, res, next) => {
    try {
      //throw new Error('Something terrible happened!');
      const patient = await records.getPatient(req.params.id);
      if (patient) {
        await records.deletePatient(patient);
        res.status(204).end();
      } else {
        res.status(404).json({message: "Patient Not Found"});
      }
    } catch (err) {
      next(err);
    }
  });
  
  
  // Send a GET request to /quotes/quote/random to READ (view) a random quote
  


  module.exports = router;