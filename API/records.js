const fs = require('fs');

function generateRandomId(){
  return Math.floor(Math.random() * 10000);
}

function save(data){
  return new Promise((resolve, reject) => {
    fs.writeFile('data.json', JSON.stringify(data, null, 2), (err) => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}

/**
 * Gets all patients
 * @param None
 */
function getPatients(){
  return new Promise((resolve, reject) => {
    fs.readFile('data.json', 'utf8', (err, data) => {
      if (err) {
        reject(err);
      } else {
        const json = JSON.parse(data);
        resolve(json);
      }
    });
  });
}

/**
 * Gets a specific patient by ID
 * @param {number} id - Accepts the ID of the specified patient.
 */
async function getPatient(id){
  const patient = await getPatients();
  return patient.records.find(record => record.id == id);
}

/**
 * Gets a random patient 
 * @param None
 */
async function getRandomPatient(){
  const patients = await getPatients();
  const randNum = Math.floor(Math.random() * patients.records.length);
  return patients.records[randNum];
}

/**
 * Creates a new patient record 
 * @param {Object} newRecord - Object containing info for new quote: the quote text, author and year 
 */
async function createPatient(newRecord) {
  const patients = await getPatients(); 
  
  newRecord.id = generateRandomId(); 
  patients.records.push(newRecord);
  await save(quotes); 
  return newRecord; 
}

/**
 * Updates a single record 
 * @param {Object} newQuote - An object containing the changes to patient: lastname, firstname, age, sexe, email
 */
async function updatePatient(newPatient){
  const patients = await getPatients();
  let patient = patients.records.find(item => item.id == newQuote.id);
  
  patient.lastname = newPatient.lastname;
  patient.firstname = newPatient.firstname;
  patient.age = newPatient.age;
  patient.sexe = newPatient.sexe;
  patient.email = newPatient.email;
 
  await save(patient);
}

/**
 * Deletes a single record
 * @param {Object} record - Accepts record to be deleted. 
 */
async function deletePatient(record){
  const patients = await getPatients();
  patients.records = patients.records.filter(item => item.id != record.id);
  await save(patients);
}

module.exports = {
  getPatients,
  getPatient, 
  createPatient, 
  updatePatient, 
  deletePatient,
  getRandomPatient
}
